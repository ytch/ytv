package com.ytch.yettv;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;

import com.ytch.yettv.json.JSONUtil;
import com.ytch.yettv.net.AsyncTaskExecuteUtil;
import com.ytch.yettv.net.HttpClientAsyncTask;

import net.htmlparser.jericho.Source;

import org.apache.commons.lang3.time.FastDateFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends Activity {
    private String mTag =this.getClass().toString();

    private long mNanoTime;
    private ViewGroup mViewGroup;
    private WebView mWebView;
    private ListView mListView;
    private Handler mHandler;
    private String mUrl;
    private float mScale;
    private SharedPreferences mPref;
    private int mUserId;
    private Boolean mIsAnimated=false;
    private Button mEndButton;


    /**
     *　このメソッドからwebViewかhttpClientでwebページのソースを取得する
     * 終了後viewSourceメソッドが呼ばれる
     * @param url 読み込むURL
     * @param isClient httpクライアントを使うかどうか
     */
    private void loadWebContents(String url, Boolean isClient) {
        if(isClient){
            AsyncTaskExecuteUtil.execute(new HttpClientAsyncTask(MainActivity.this),url);
        }else{
            mWebView.loadUrl(url);
        }
    }


    /**
     * JavaScriptから呼ばせる関数、onPageFinished時に呼ばせる これによりWebView内のHTMLソースを取得する
     * またそのソースとURLにより処理を行う
     * @param src
     */
    @JavascriptInterface
    public void viewSource(final String src){
        boolean post = mHandler.post(new Runnable() {
            @Override
            public void run() {
                System.out.println(mUrl);
                if (mUrl.indexOf("/tv_shows/")>0||mUrl.indexOf("/first/")>0) {

                    mPref.edit().putInt("user_id",mUserId).commit();
                    //リダイレクト先URL
                    // view.setVisibility(View.INVISIBLE);
                    mViewGroup.addView(mListView);
                    Source segments = new Source(src);

                    JSONArray jsonArray = JSONUtil.getJSONArrayOnHtml(segments);
                    TvDataAdapter results = new TvDataAdapter(MainActivity.this, jsonArray);
                    mListView.setAdapter(results);
                    ColorDrawable colorDrawable = new ColorDrawable(Color.rgb(238, 238, 238));
                    mListView.setDivider(colorDrawable);
                    mListView.setDividerHeight(40);
                    mListView.setFooterDividersEnabled(false);

                    findViewById(R.id.first_progress).setVisibility(View.GONE);
                    mEndButton.setEnabled(true);
                    mEndButton.setTextColor(Color.parseColor("#FFFFFF"));
                    mEndButton.setBackgroundColor(getResources().getColor(R.color.base_color));
                    mEndButton.setText("完了");


                    //アニメーション用

                    /*
                    mListView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            if(!mIsAnimated){
                                for(int i=0;i<mListView.getChildCount();i++){
                                    View view = mListView.getChildAt(i);
                                    if(null==view.getTag(R.string.is_animated)){
                                        int height = view.getHeight();
                                        AnimationSet animationSet=new AnimationSet(false);
                                        Animation animation = null;
                                        animation = new AlphaAnimation(0,1);
                                        animation.setDuration(10);
                                        animationSet.addAnimation(animation);

                                        animation = new TranslateAnimation(0,0,-height,0);
                                        animation.setDuration(1000);
                                        animationSet.addAnimation(animation);
                                        animationSet.setStartOffset(300 * i);
                                        animationSet.setDuration(1000);
                                        view.startAnimation(animationSet);
                                        view.setTag(R.string.is_animated,true);
                                    }
                                }
                            }
                            mIsAnimated=true;
                        }
                    });
                    mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getApplicationContext(),PageActivity.class);
                            String title = null;
                            try {
                                title = ((JSONObject)parent.getAdapter().getItem(position)).getString("title");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            intent.putExtra("title",title);


                            String text = null;
                            try {
                                text = ((JSONObject)parent.getAdapter().getItem(position)).getString("description");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            intent.putExtra("description",text);

                            String station = null;
                            try {
                                station = ((JSONObject)parent.getAdapter().getItem(position)).getString("station");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            intent.putExtra("station",station );




                            FastDateFormat fastDateFormat =
                                    org.apache.commons.lang3.time.DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT;
                            String patterns[] = { fastDateFormat.getPattern() };

                            String time = null;
                            try {
                                time = ((JSONObject)parent.getAdapter().getItem(position)).getString("start");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Date dstDate = null;
                            try {
                                dstDate = org.apache.commons.lang3.time.DateUtils.parseDate(
                                        time, patterns);
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                            SimpleDateFormat dstDateFormat = new SimpleDateFormat(
                                        "M月d日（EEE）HH時mm分");
                            String dstString = dstDateFormat.format(dstDate);
                            intent.putExtra("date",dstString);
                            intent.putExtra("time",time);


                            startActivity(intent);
                        }
                    });
                    */

                    for (int i=0;i<jsonArray.length();i++){
                        try {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            makeAlram(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                }else if(mUrl.indexOf("/auth/twitter/callback")>0){


                    ScrollView scrollView= (ScrollView) findViewById(R.id.scroll_view);

                    scrollView.setVisibility(View.VISIBLE);


                    mViewGroup.removeView(mWebView);
                    JSONObject jsonObjectOnHtml = JSONUtil.getJSONObjectOnHtml(new Source(src));
                    try {
                        mUserId=jsonObjectOnHtml.getInt("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    loadWebContents("http://krswfmy.com:3000/users/"+mUserId+"/first/013", true);
                }
            }
        });
    }

    private void makeAlram(JSONObject jsonObject) throws JSONException, ParseException {
        Intent intent = new Intent(getApplicationContext(), ReceivedActivity.class); // ReceivedActivityを呼び出すインテントを作成
        intent.putExtra("title",jsonObject.getString("title"));
        intent.putExtra("start",jsonObject.getString("start"));
        int requestID = (int) System.currentTimeMillis();
        PendingIntent sender = PendingIntent.getBroadcast(this, requestID, intent, 0); // ブロードキャストを投げるPendingIntentの作成

        Calendar calendar = Calendar.getInstance(); // Calendar取得
        calendar.setTimeInMillis(System.currentTimeMillis()); // 現在時刻を取得
        String start = jsonObject.getString("start");
        FastDateFormat fastDateFormat =
                org.apache.commons.lang3.time.DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT;
        String patterns[] = { fastDateFormat.getPattern() };
        try {
            Date date = org.apache.commons.lang3.time.DateUtils.parseDate(start, patterns);
            /*
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date = simpleDateFormat.parse(start);
            */
            calendar.setTime(date);
            calendar.add(Calendar.SECOND, -60); // 現時刻より15秒後を設定

            AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE); // AlramManager取得
            am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender); // AlramManagerにPendingIntentを登録
        }catch (Exception e){

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHandler=new Handler();
        mScale = getResources().getDisplayMetrics().density;
        mNanoTime =System.nanoTime();
        mPref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        mListView = new ListView(MainActivity.this);
        mEndButton=(Button)findViewById(R.id.end_tutorial);

        mEndButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            findViewById(R.id.scroll_view).setVisibility(View.GONE);
                            mListView.setVisibility(View.VISIBLE);
                        }
                    });
        mWebView= (WebView) findViewById(R.id.webView);
        mViewGroup = (ViewGroup) mWebView.getParent();
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(this, "activity" + String.valueOf(mNanoTime));
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mUrl=url;
                view.requestFocus(View.FOCUS_DOWN);
                view.loadUrl("javascript:window.activity" + String.valueOf(mNanoTime) + ".viewSource(document.documentElement.outerHTML);");

            }
        });
        /*
        mWebView.setDownloadListener(new DownloadListener()
        {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength)
            {
                Log.i("OF", "onDownloadStart(): " + " url = " + url );
                Log.i("OF", " userAgent = " + userAgent );
                Log.i("OF", " contentDisposition = " + contentDisposition );
                Log.i("OF", " mimetype = " + mimetype );
                Log.i("OF", " contentLength (KB) = " + (contentLength /
                        1024.0) );
            }
        });
        */
        mUserId=mPref.getInt("user_id",-1);
        if(mUserId==-1){
            mListView.setVisibility(View.GONE);
            LayoutInflater inflater
                    = LayoutInflater.from(MainActivity.this);
            View view = inflater.inflate(R.layout.dialog_layout, null);

            loadWebContents("http://krswfmy.com:3000/login", false);
        }else{



            loadWebContents("http://krswfmy.com:3000/users/" + mUserId + "/tv_shows/013", true);
            //loadWebContents("http://krswfmy.com:3000/tv/get" , true);
        }
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
