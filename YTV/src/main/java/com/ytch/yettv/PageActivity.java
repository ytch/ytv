package com.ytch.yettv;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

/**
 * Created by takam on 2013/06/14.
 */
public class PageActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.page_layout);
        Bundle extras;
        if(getIntent()!=null&&(extras = getIntent().getExtras())!=null){
            ((TextView) findViewById(R.id.title)).setText(extras.getString("title","取得出来ませんでした"));
            ((TextView) findViewById(R.id.start)).setText(extras.getString("date","取得出来ませんでした"));
            ((TextView) findViewById(R.id.station)).setText(extras.getString("station","取得出来ませんでした"));
            ((TextView) findViewById(R.id.description)).setText(extras.getString("description","取得出来ませんでした"));
        }
    }
}
