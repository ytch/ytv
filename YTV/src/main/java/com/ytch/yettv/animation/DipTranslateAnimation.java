package com.ytch.yettv.animation;

import android.view.animation.TranslateAnimation;


/**
 * Created by takam on 2013/05/31.
 */
public class DipTranslateAnimation extends TranslateAnimation {

    public DipTranslateAnimation(float scale,int fromX, int toX, int fromY, int toY) {
        super(fromX*scale, toX*scale, fromY*scale, toY*scale);
    }

}

