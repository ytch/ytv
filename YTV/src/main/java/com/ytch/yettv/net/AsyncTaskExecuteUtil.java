package com.ytch.yettv.net;

import java.util.concurrent.RejectedExecutionException;

import android.os.AsyncTask;
import android.os.Build;
import android.view.View;

public class AsyncTaskExecuteUtil <T1,T2>{

	public static <T1,T2> void execute(AsyncTask<String, T1, T2> task, String arg){
		try{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
				task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,arg);
			}else{
				task.execute(arg);
			}
		} catch (RejectedExecutionException e) {
			// TODO: handle exception
			
		}
	}

}
