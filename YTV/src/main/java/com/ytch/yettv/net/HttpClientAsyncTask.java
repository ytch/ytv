package com.ytch.yettv.net;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.ytch.yettv.MainActivity;
import com.ytch.yettv.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
public class HttpClientAsyncTask extends AsyncTask<String,Object,String> {
	/**
	 * 
	 */
	private MainActivity mActivity;

	/**
	 * 
	 */
    private ProgressBar mProgressBar;

	/**
	 * コンストラクタ
	 * @param activity
	 */
	public HttpClientAsyncTask(MainActivity activity) {
		mActivity = activity;
        mProgressBar= (ProgressBar) mActivity.findViewById(R.id.progressBar);
	}


	/* (非 Javadoc)
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		mProgressBar.setVisibility(View.VISIBLE);
		
	}

	/* (非 Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected String doInBackground(String... params) {
		String url = params[0];	
		HttpGet request = new HttpGet(url);

		// HttpClientインタフェースではなくて、実クラスのDefaultHttpClientを使う。
		// 実クラスでないとCookieが使えないなど不都合が多い。
		DefaultHttpClient httpClient = new DefaultHttpClient();


		String result =null;
		try {
		    result = httpClient.execute(request, new ResponseHandler<String>() {
		        @Override
		        public String handleResponse(HttpResponse response)
		                throws ClientProtocolException, IOException {
		            
		            // response.getStatusLine().getStatusCode()でレスポンスコードを判定する。
		            // 正常に通信できた場合、HttpStatus.SC_OK（HTTP 200）となる。
		        	/*
		        	switch (response.getStatusLine().getStatusCode()) {
		        	case HttpStatus.SC_OK:
		        		// レスポンスデータを文字列として取得する。
		        		// byte[]として読み出したいときはEntityUtils.toByteArray()を使う。
		        		*/
		        	if(response.getStatusLine().getStatusCode()==HttpStatus.SC_OK){
		        		return EntityUtils.toString(response.getEntity(), "UTF-8");
		        	}
		        	return null;
/*
		        	case HttpStatus.SC_NOT_FOUND:
		        		throw new RuntimeException("データないよ！"); //FIXME

		        	default:
		        		throw new RuntimeException("なんか通信エラーでた"); //FIXME
		        	}
		        	*/	

		        }
		    });

		} catch (ClientProtocolException e) {
		   // throw new RuntimeException(e); //FIXME
		} catch (IOException e) {
		   // throw new RuntimeException(e); //FIXME
		} finally {
		    // ここではfinallyでshutdown()しているが、HttpClientを使い回す場合は、
		    // 適切なところで行うこと。当然だがshutdown()したインスタンスは通信できなくなる。
		    httpClient.getConnectionManager().shutdown();
		}
        mActivity.setUrl(url);
		return result;
	}

	

	/* (非 Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(String result) {

		mActivity.viewSource(result);
		mProgressBar.setVisibility(View.INVISIBLE);
	}
}