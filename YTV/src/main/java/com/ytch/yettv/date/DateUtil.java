package com.ytch.yettv.date;

import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.http.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by takam on 2013/06/15.
 */
public class DateUtil {
    static public String getDateFromString(String time){
        FastDateFormat fastDateFormat =
                org.apache.commons.lang3.time.DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT;
        String patterns[] = { fastDateFormat.getPattern() };

        Date dstDate = null;
        try {
            dstDate = org.apache.commons.lang3.time.DateUtils.parseDate(
                    time, patterns);
        } catch (ParseException e1) {
            e1.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dstDateFormat = new SimpleDateFormat(
                "M月d日（EEE）HH時mm分");
        String dstString = dstDateFormat.format(dstDate);
        return dstString;
    }
}
