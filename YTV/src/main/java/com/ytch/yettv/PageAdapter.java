package com.ytch.yettv;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Date;

/**
 * Created by takam on 2013/06/14.
 */
public class PageAdapter extends PagerAdapter{
    LayoutInflater mLayoutInflater;
    public PageAdapter(Context context) {
        mLayoutInflater= LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view.equals(o);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        FrameLayout layout = (FrameLayout) mLayoutInflater.inflate(R.layout.table_row, null);
        TextView textView = (TextView) layout.findViewById(R.id.title);



            textView.setText(String.valueOf(position)+"test");
        container.addView(layout);
        return layout;
    }
}
