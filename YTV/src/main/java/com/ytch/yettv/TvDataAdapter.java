package com.ytch.yettv;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ytch.yettv.date.DateUtil;

import org.apache.commons.lang3.time.FastDateFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by takam on 2013/06/08.
 */
public class TvDataAdapter extends BaseAdapter {
    Context mContext;
    JSONArray mJSONArray;
    float touchStartX;
    private SharedPreferences mPref;

    private void makeAlram(JSONObject jsonObject) throws JSONException, ParseException {
        Intent intent = new Intent(mContext, ReceivedActivity.class); // ReceivedActivityを呼び出すインテントを作成
        intent.putExtra("title",jsonObject.getString("title"));
        intent.putExtra("description",jsonObject.getString("description"));
        intent.putExtra("start",jsonObject.getString("start"));
        int requestID = (int) System.currentTimeMillis();
        PendingIntent sender = PendingIntent.getBroadcast(mContext, requestID, intent, 0); // ブロードキャストを投げるPendingIntentの作成

        Calendar calendar = Calendar.getInstance(); // Calendar取得
        calendar.setTimeInMillis(System.currentTimeMillis()); // 現在時刻を取得
        String start = jsonObject.getString("start");
        FastDateFormat fastDateFormat =
                org.apache.commons.lang3.time.DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT;
        String patterns[] = { fastDateFormat.getPattern() };
        try {
            Date date = org.apache.commons.lang3.time.DateUtils.parseDate(start, patterns);
            /*
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date = simpleDateFormat.parse(start);
            */
            calendar.setTime(date);
            calendar.add(Calendar.SECOND, -60); // 現時刻より15秒後を設定

            AlarmManager am = (AlarmManager)mContext.getSystemService(mContext.ALARM_SERVICE); // AlramManager取得
            am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender); // AlramManagerにPendingIntentを登録
        }catch (Exception e){

        }
    }

    public TvDataAdapter(Context context, JSONArray jsonArray) {
        mContext=context;
        mJSONArray=jsonArray;
    }


    @Override
    public int getCount() {
        return mJSONArray.length();
    }


    @Override
    public JSONObject getItem(int position) {
        try {
            return (JSONObject) mJSONArray.get(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        mPref = PreferenceManager.getDefaultSharedPreferences(mContext);

        LayoutInflater inf = LayoutInflater.from(mContext);
        final View v = inf.inflate(R.layout.table_row, null);

        final RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.expand_layout);
        try {
            if(getItem(position).has("notification")||mPref.getBoolean("ytch_listview_cache"+getItem(position).getString("title")+getItem(position).getString("description"),false)){
                 v.findViewById(R.id.notification_button).setEnabled(false);
                v.findViewById(R.id.notification_button).setBackgroundColor(Color.parseColor("#CBCBCB"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        v.findViewById(R.id.notification_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Notificationインスタンスの生成と設定

                try {
                    getItem(position).put("notification",true);
                    mPref.edit().putBoolean("ytch_listview_cache"+getItem(position).getString("title")+getItem(position).getString("description"),true).commit();
                    TvDataAdapter.this.makeAlram(getItem(position));
                    v.setEnabled(false);
                    v.setBackgroundColor(Color.parseColor("#CBCBCB"));
                    Toast.makeText(mContext,"時間になったら通知します",Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        v.findViewById(R.id.tweet_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                try {
                    i = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://twitter.com/intent/tweet?text=『" + getItem(position).getString("title") + "』なんて見たくない"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mContext.startActivity(i);
            }
        });
        v.findViewById(R.id.touch_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (relativeLayout.getVisibility() == View.GONE) {
                    relativeLayout.setVisibility(View.VISIBLE);
                } else if (relativeLayout.getVisibility() == View.VISIBLE) {
                    relativeLayout.setVisibility(View.GONE);
                }
            }
        });

        //v.setBackgroundColor(Color.rgb(206,206,206));
        //v.setBackgroundResource(R.drawable.brogdropshadow);
        v.setPadding(30,30,30,30);


        String title = null;
        try {
            title = getItem(position).getString("title");

            TextView titleText = (TextView) v.findViewById(R.id.title);
            titleText.setText(title);
            String text = getItem(position).getString("description");
            TextView contents = (TextView) v.findViewById(R.id.description);
            contents.setText(text);

            String start = DateUtil.getDateFromString(getItem(position).getString("start"));

            TextView timeText = (TextView) v.findViewById(R.id.start);
            timeText.setText(start);

            String station = getItem(position).getString("station");
            TextView stationText = (TextView) v.findViewById(R.id.station);
            stationText.setText(station);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*
        final LinearLayout linearLayout =(LinearLayout) v.findViewById(R.id.linear_layout);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("com.kogitune",event.toString());
                if(event.getAction()== MotionEvent.ACTION_DOWN){
                    touchStartX=event.getX();
                }else if(event.getAction()== MotionEvent.ACTION_MOVE){
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) linearLayout.getLayoutParams();
                    layoutParams.leftMargin= (int) (event.getRawX()-touchStartX);
                    layoutParams.rightMargin= (int) (touchStartX-event.getRawX());
                    linearLayout.setLayoutParams(layoutParams);
                    //linearLayout.layout((int)(event.getX()-touchStartX)+layoutParams.leftMargin,layoutParams.topMargin,(int)(event.getX()-touchStartX+layoutParams.width)+layoutParams.rightMargin,layoutParams.height+layoutParams.bottomMargin);
                }else if(event.getAction()== MotionEvent.ACTION_UP){
                    if(event.getRawX()-touchStartX>v.getMeasuredWidth()/2){
                        //右にはみ出した時の処理

                    }else if(event.getRawX()-touchStartX>-v.getMeasuredWidth()/2){
                        //左にはみだしたときの処理

                     }else{
                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) linearLayout.getLayoutParams();
                    }
                }
                return true;
            }
        });
        */
        return v;
    }

}
