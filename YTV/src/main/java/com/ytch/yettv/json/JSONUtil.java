package com.ytch.yettv.json;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by takam on 2013/06/13.
 */
public class JSONUtil {
    /**
     * HTMLのソースからjsonの文字列を抽出しJSONArrayを生成する
     * @param source new Source(WebView内にあったHTMLソース)
     * @return JSONObject JSONArrayを返す
     */
    public static JSONArray getJSONArrayOnHtml(Source source) {

        JSONArray jsonArray=null;
        List<Element> preStartTags=source.getAllElements(HTMLElementName.PRE);
        if(preStartTags.size()>0){
            Element element=preStartTags.get(0);
            try {
                jsonArray = new JSONArray(element.getContent().getTextExtractor().toString());
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }else{
            try {
                jsonArray = new JSONArray(source.getTextExtractor().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return jsonArray;
    }
    /**
     * HTMLのソースからjsonの文字列を抽出しJSONObjectを生成する
     * @param source new Source(WebView内にあったHTMLソース)
     * @return JSONObject JSONObjectを返す
     */
    public static JSONObject getJSONObjectOnHtml(Source source) {

        JSONObject jsonObject=null;
        List<Element> preStartTags=source.getAllElements(HTMLElementName.PRE);

        if(preStartTags.size()>0){
            Element element=preStartTags.get(0);
            try {
                jsonObject = new JSONObject(element.getContent().getTextExtractor().toString());
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }else{
         try {
                jsonObject = new JSONObject(source.getTextExtractor().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return jsonObject;
    }
}
