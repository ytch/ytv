package com.ytch.yettv;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;

import com.ytch.yettv.date.DateUtil;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Date;
import java.util.Random;

/**
 * Created by takam on 2013/06/10.
 */
public class ReceivedActivity extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        //Notificationインスタンスの生成と設定
        Notification n = new Notification(); // Notificationの生成
        n.icon = R.drawable.ic_launcher; // アイコンの設定
        n.tickerText = "..."; // メッセージの設定

        Intent i = new Intent(context.getApplicationContext(), MainActivity.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);

        String title=extras.getString("title");
        String description=extras.getString("description");
        String start= DateUtil.getDateFromString(extras.getString("start"));


        n.setLatestEventInfo(context.getApplicationContext(), title, description+start, pi);

        /*
        long[] vibrate_ptn = {0, 100, 300, 1000}; // 独自バイブレーションパターン
        n.vibrate = vibrate_ptn; // 独自バイブレーションパターンを設定
        */
        n.defaults |= Notification.DEFAULT_LIGHTS; // デフォルトLED点滅パターンを設定

        // NotificationManagerのインスタンス取得
        NotificationManager nm =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(1, n); // 設定したNotificationを通知する
    }
}
